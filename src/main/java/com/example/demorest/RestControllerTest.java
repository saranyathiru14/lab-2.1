package com.example.demorest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.time.LocalTime;


@RestController
public class RestControllerTest {

    @RequestMapping("/time")
    public String getTime() {
        return "Current Time: " + LocalTime.now().toString();
    }

    @RequestMapping("/date")
    public String getDate() {
        return "Current Date: " + LocalDate.now().toString();
    }

    @RequestMapping("/andy")
    public String getAndy() {
        return "Hey Andy! Fancy you landed on this page!";
    }



}
